#!/bin/bash
set -e

if [ ! -d /var/lib/mysql/mysql ]; then
    mysql_install_db --datadir=/var/lib/mysql
fi

if [ "${1:0:1}" = '-' ]; then
    set -- mysqld "$@"
fi

exec "$@"
