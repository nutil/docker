# galera/mariabackup

3 node mariadb galera cluster using docker on a single machine (your laptop).

dev/test mariabackup sst method
```
wsrep_sst_method = mariabackup
```

https://mariadb.com/kb/en/mariabackup-sst-method/     

---

## local setup, mac osx in my case,
```
mkdir -p /opt/data/conf.d
mkdir /opt/data/node1
mkdir /opt/data/node2
mkdir /opt/data/node3

cat <<-EOE>/opt/data/conf.d/my.cnf
[mysqld]
port = 3306
bind-address = 0.0.0.0
default_storage_engine = innodb
binlog_format = row
innodb_autoinc_lock_mode = 2
innodb_flush_log_at_trx_commit = 0
query_cache_size = 0
query_cache_type = 0

[galera]
wsrep_on = ON
wsrep_provider = /usr/lib64/galera-4/libgalera_smm.so
wsrep_sst_method = mariabackup
wsrep-sst-auth = mariabackup:mariabackup
wsrep-cluster-name = galera-cluster
wsrep_cluster_address = gcomm://node1,node2,node3

EOE
```

## build docker image
```
docker build -t centos:galera-node ./
```

## run node1 (initializing a new cluster)
```
docker run --detach=false --name node1 -h node1 -p 3306:3306 \
  -v /opt/data/conf.d:/etc/my.cnf.d \
  -v /opt/data/node1:/var/lib/mysql \
  centos:galera-node \
  --wsrep-new-cluster
```

## run grants for mariabackup
```
docker exec -it --user root node1 /usr/bin/mysql -e "CREATE USER 'mariabackup'@'localhost' IDENTIFIED BY 'mariabackup';"
docker exec -it --user root node1 /usr/bin/mysql -e "GRANT RELOAD, PROCESS, LOCK TABLES, REPLICATION CLIENT ON *.* TO 'mariabackup'@'localhost';"
```

## run node2, joining the cluster
```
docker run --detach=false --name node2 -h node2 -p 3307:3306 \
  --link node1:node1 \
  -v /opt/data/conf.d:/etc/my.cnf.d \
  -v /opt/data/node2:/var/lib/mysql \
  centos:galera-node \
  --wsrep-sst-donor=node1
```

## run node3, join the cluster via node2 doner
```
docker run --detach=false --name node3 -h node3 -p 3308:3306 \
  --link node1:node1 \
  --link node2:node2 \
  -v /opt/data/conf.d:/etc/my.cnf.d \
  -v /opt/data/node3:/var/lib/mysql \
  centos:galera-node \
  --wsrep-sst-donor=node2
```

# Galera Cluster
Once the cluster has successfully initialized, you can continue to join additional nodes.  Should the cluster ever terminate (all nodes shutdown), galera has a procedure for identifying and starting the first node to get the cluster going again.  
https://galeracluster.com/library/training/tutorials/restarting-cluster.html

   


