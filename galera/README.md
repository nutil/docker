# 3 node mariadb galera cluster using docker on a single machine (your laptop)

## build a docker container image
```
docker build -t centos:galera-node1 ./    
docker build -t centos:galera-node2 ./    
docker build -t centos:galera-node3 ./    
```

## run containers
```
docker run --detach=false --name node1 -h node1 centos:galera-node1 --wsrep-new-cluster --wsrep-cluster-name=galera-cluster --wsrep-cluster-address=gcomm://

docker run --detach=false --name node2 -h node2 --link node1:node1 centos:galera-node2 --wsrep-cluster-name=galera-cluster --wsrep-cluster-address=gcomm://node1

docker run --detach=false --name node3 -h node3 --link node1:node1 centos:galera-node3 --wsrep-cluster-name=galera-cluster --wsrep-cluster-address=gcomm://node1
```

# connect to docker container instance
```
docker exec -it node1 /usr/bin/mysql -e "show status"
```

## mariadb:latest

We'll use standard docker image mariadb:latest    
  - we'll docker mount local filesystem for persistent data and my.cnf     
  - we'll docker --link the three nodes for internal networking (tcp 4567 4568 4444)     
  - we'll expose each internal mysql port 3306 as node1:3306, node2:3307, node3:3308     

https://hub.docker.com/_/mariadb
docker image: mariadb:latest (latest as of this writing: 10.5.4)

---

local setup, mac osx in my case,
```
mkdir -p /opt/data/conf.d
mkdir /opt/data/node1
mkdir /opt/data/node2
mkdir /opt/data/node3

cat <<-EOE>/opt/data/conf.d/my.cnf
[mysqld]
port = 3306
bind-address = 0.0.0.0
default_storage_engine = innodb
binlog_format = row
innodb_autoinc_lock_mode = 2
innodb_flush_log_at_trx_commit = 0
query_cache_size = 0
query_cache_type = 0

wsrep_on = ON
wsrep_provider = /usr/lib/galera/libgalera_smm.so
wsrep_sst_method = rsync
EOE

```

## Run via docker-compose.yml
```
node1:
    image: mariadb:latest
    hostname: node1
    volumes:
      - /opt/data/conf.d:/etc/mysql/conf.d
      - /opt/data/node1:/var/lib/mysql
    ports:
      - 3306:3306
    environment:
      - MYSQL_ROOT_PASSWORD=galera
      - GALERA=On
      - NODE_NAME=node1
    command: --wsrep-new-cluster --wsrep-cluster-name=galera-cluster --wsrep-cluster-address=gcomm://

node2:
    image: mariadb:latest
    hostname: node2
    volumes:
      - /opt/data/conf.d:/etc/mysql/conf.d
      - /opt/data/node2:/var/lib/mysql
    links:
      - node1
    ports:
      - 3307:3306
    environment:
      - MYSQL_ROOT_PASSWORD=galera
      - GALERA=On
      - NODE_NAME=node2
    command: --wsrep-cluster-name=galera-cluster --wsrep-cluster-address=gcomm://node1

node3:
    image: mariadb:latest
    hostname: node3
    volumes:
      - /opt/data/conf.d:/etc/mysql/conf.d
      - /opt/data/node3:/var/lib/mysql
    links:
      - node1
    ports:
      - 3308:3306
    environment:
      - MYSQL_ROOT_PASSWORD=galera
      - GALERA=On
      - NODE_NAME=node3
    command: --wsrep-cluster-name=galera-cluster --wsrep-cluster-address=gcomm://node1
```
```
docker-compose up
```

## run node1
```
docker-compose -f docker-compose.yml up node1
```
## run node2
```
docker-compose -f docker-compose.yml up node2
```
## run node3
```
docker-compose -f docker-compose.yml up node3
```


# You can run each node via cli

## start the first node --wsrep-new-cluster (initializing a new cluster)
```
docker run --detach=true --name node1 -h node1 -p 3306:3306 \
  -e MYSQL_ROOT_PASSWORD=galera \
  -v /opt/data/conf.d:/etc/mysql/conf.d \
  -v /opt/data/node1:/var/lib/mysql \
  mariadb:latest \
  --wsrep-new-cluster --wsrep-cluster-name=galera-cluster --wsrep-cluster-address=gcomm://
```
docker logs node1 -f     
docker exec -it node1 /bin/bash     
mysql -h 127.0.0.1 -P 3306 -u root -pgalera     

## run 2nd node, joining the cluster
```
docker run --detach=true --name node2 -h node2 -p 3307:3306 \
  --link node1:node1 \
  -e MYSQL_ROOT_PASSWORD=galera \
  -v /opt/data/conf.d:/etc/mysql/conf.d \
  -v /opt/data/node2:/var/lib/mysql \
  mariadb:latest \
  --wsrep-cluster-name=galera-cluster --wsrep-cluster-address=gcomm://node1
```
docker logs node2 -f     
docker exec -it node2 /bin/bash     
mysql -h 127.0.0.1 -P 3307 -u root -pgalera     

## run 3rd node, joining the cluster
```
docker run --detach=true --name node3 -h node3 -p 3308:3306 \
  --link node1:node1 \
  -e MYSQL_ROOT_PASSWORD=galera \
  -v /opt/data/conf.d:/etc/mysql/conf.d \
  -v /opt/data/node3:/var/lib/mysql \
  mariadb:latest \
  --wsrep-cluster-name=galera-cluster --wsrep-cluster-address=gcomm://node1
```
docker logs node3 -f    
docker exec -it node3 /bin/bash     
mysql -h 127.0.0.1 -P 3308 -u root -pgalera     

docker inspect -f "{{ .HostConfig.Links }}" node3     

Note: docker --link is now considered legacy https://docs.docker.com/network/links/     

# Galera Cluster
Once the cluster has successfully initialized, you can continue to join additional nodes.  Should the cluster ever terminate (all nodes shutdown), galera has a procedure for identifying and starting the first node to get the cluster going again.  
https://galeracluster.com/library/training/tutorials/restarting-cluster.html      

---

https://galeracluster.com/library/documentation/docker.html     



